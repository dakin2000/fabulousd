from django.conf.urls import url
from django.contrib import admin

from .views import HomeView,CatalogView,CatalogDetailView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^detail/$', CatalogDetailView.as_view(), name='detail'),
    url(r'^catalog/$', CatalogView.as_view(), name='catalog'),
    # url(r'^catalog/(?P<slug>[w]+)$', DetailView.as_view(), name='detail')
]
