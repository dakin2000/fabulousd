# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from .models import Product,Category

# Create your views here.
from django.views.generic import TemplateView,ListView,DetailView

class HomeView(TemplateView):
	template_name = 'index.html'
	queryset = Product.objects.all()
	model = Product

	def get_context_data(self, **kwargs):
	    context = super(HomeView, self).get_context_data(**kwargs)
	    context['products'] = self.queryset
	    return context

	# def get_context_data(self)CatalogView


class CatalogView(ListView):
	model = Product
	template_name = 'catalog.html'
	page_title = 'Cloths and other designer wears'

	def get_context_data(self, **kwargs):
	    # Call the base implementation first to get a context
	    context = super(CatalogView, self).get_context_data(**kwargs)
	    # Add in the publisher
	    # context['products'] = self.name
	    context['title'] = self.page_title

	    return context


class CatalogDetailView(DetailView):
	template_name = 'detail.html'
	model = Product

	def get_context_data(self, *args, **kwargs):
	    context = super(CatalogDetailView, self).get_context_data(*args, **kwargs)
	    context['now'] = timezone.now()
	    return context

	# def get_slug_field(self, **kwargs):
	# 	pass
