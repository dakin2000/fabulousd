from django.conf.urls import url

from .views import CartView,add_to_cart,remove_from_cart

urlpatterns = [
    url(r'^$', CartView.as_view(), name='cart_view'),
    url(r'^add/(?P<slug>[\w-]+)/$', add_to_cart, name='add_to_cart'),
    url(r'^remove/(?P<id>\d+)/$', remove_from_cart, name='remove_from_cart'),

    # url(r'^catalog/(?P<slug>[w]+)$', DetailView.as_view(), name='detail')
]
