# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Cart,CartItem

class CartAdmin(admin.ModelAdmin):
	list_display = ['__unicode__', 'total_price', 'is_active', 'created_at', 'updated_at',]
	list_display_links = ['__unicode__',]
	list_per_page = 50
	ordering = ['-created_at']
	search_fields = ['is_active', 'created_at',]
	exclude = ['created_at', 'updated_at']

	class Meta:
		model = Cart

admin.site.register(Cart,CartAdmin)
admin.site.register(CartItem)