# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from products.models import Product,Variation

class CartItem(models.Model):
	cart = models.ForeignKey('Cart', null=True)
	product = models.ForeignKey(Product)
	variations = models.ManyToManyField(Variation, blank=True)
	quantity = models.IntegerField(default=1)
	line_total = models.DecimalField(max_digits=9, decimal_places=2, default=0.00)
	notes = models.TextField(null=True, blank=True)
	# Create your models here.

	def __unicode__(self):
		try:
			return str(self.cart.id)
		except:
			return self.product.name

	def __str__(self):
		try:
			return str(self.cart.id)
		except:
			return self.product.name

class Cart(models.Model):
	total_price = models.DecimalField(max_digits=9,decimal_places=2, default=0.00)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	is_active = models.BooleanField(default=True)


	def __unicode__(self):
		return "Card ID is : {0}".format(self.id)

	def __str__(self):
		return "Card ID is : {0}".format(self.id)