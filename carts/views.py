# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.shortcuts import render,HttpResponseRedirect
from django.views.generic import ListView

from .models import Cart, CartItem
from products.models import Product,Variation

# Create your views here.
class CartView(ListView):
	template_name = 'product_cart.html'
	# queryset = Product.objects.all()
	model = Cart

	def get_context_data(self, **kwargs):
		context = super(CartView, self).get_context_data(**kwargs)
		try:
			the_id = self.request.session['cart_id']
			cart = Cart.objects.get(id=the_id)
		except:
			the_id = None
		if the_id:
			new_total = 0.00
			for item in cart.cartitem_set.all():
				line_total = float(item.product.price) * item.quantity
				new_total += line_total
			self.request.session['items_total'] = cart.cartitem_set.count()
			self.request.session['total_price'] = new_total
			cart.total_price = new_total
			cart.save()
			context['cart'] = cart
		else:
			context['empty_message'] = 'Your Cart is empty, pls keep shoping'
			context['empty'] = True
		# context['products'] = self.queryset
		return context


def remove_from_cart(request,id):
	try:
		the_id = request.session['cart_id']
		cart = Cart.objects.get(id=the_id)
	except:
		return HttpResponseRedirect(reverse('carts:cart_view'))

	cart_item = CartItem.objects.get(id=id)
	# cart_item.delete()
	cart_item.cart = None
	cart_item.save()

	return HttpResponseRedirect(reverse('carts:cart_view'))

def add_to_cart(request, slug):
	try:
		the_id = request.session['cart_id']
	except:
		new_cart = Cart()
		new_cart.save()
		request.session['cart_id'] = new_cart.id
		the_id = new_cart.id

	cart = Cart.objects.get(id=the_id)


	try:
		product = Product.objects.get(slug=slug)
	except Product.DoesNotExist:
		pass
	except:
		pass

	product_variation = []
	if request.method == "POST":
		qty = request.POST['qty']
		for item in request.POST:
			key = item
			val = request.POST[key]
			try:
				v = Variation.objects.get(product=product,category__iexact=key,title__iexact=val)
				product_variation.append(v)
				print 'hello ' + str(v)
				print qty
			except:
				pass	

		cart_item = CartItem.objects.create(cart=cart, product=product)

		if len(product_variation) > 0:
			cart_item.variations.add(*product_variation)
		cart_item.quantity = qty
		# cart_item.notes = note
		cart_item.save()

		return HttpResponseRedirect(reverse('carts:cart_view'))

	return HttpResponseRedirect(reverse('carts:cart_view'))