from django.conf.urls import include,url
from .views import check_out,OrderListView

urlpatterns = [
    url(r'^check_out/', check_out, name='check_out'),
    url(r'^', OrderListView.as_view(), name='order_list'),
]