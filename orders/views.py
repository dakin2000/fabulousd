# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import time
from django.views.generic import ListView
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render,HttpResponseRedirect
from carts.models import Cart
from .models import Order
from .utils import id_generator

class OrderListView(ListView):
	# template_name = 'products/product_list.html'
	template_name = 'order/order_list.html'
	model = Order

	def get_context_data(self, **kwargs):
	    context = super(OrderListView, self).get_context_data(**kwargs)
	    context['title'] = 'Orders'
	    return context


# Create your views here.
@login_required
def check_out(request):
	try:
		the_id = request.session['cart_id']
		cart = Cart.objects.get(id=the_id)
	except:
		the_id = None
		return HttpResponseRedirect(reverse('carts:cart_view'))

	new_order, created = Order.objects.get_or_create(cart=cart)
	if created:
		print 'hellohellohellohellohellohello'
		new_order.order_id = id_generator(size=10)
		new_order.save()

	new_order.user = request.user
	new_order.save()
	if new_order.status == 'Finished':
		del request.session['cart_id']
		del	request.session['items_total']
		del	request.session['total_price']

	context = {

	}
	return render(request,'check_out.html',context)

