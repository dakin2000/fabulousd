# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth import get_user_model

from django.db import models

# Create your models here.
from carts.models import Cart

User = get_user_model()
STATUS_CHOICES = (
		('Started','Started'),
		('Abandoned','Abandoned'),
		('Finished','Finished'),
	)

class Order(models.Model):
	user = models.ForeignKey(User, blank=True, null=True)
	order_id = models.CharField(max_length=120, unique=True)
	cart = models.ForeignKey(Cart)
	status = models.CharField(max_length=120, choices=STATUS_CHOICES, default='Started')
	sub_total = models.DecimalField(max_digits=9, decimal_places=2, default=0.00)
	tax_total = models.DecimalField(max_digits=9, decimal_places=2, default=0.00)
	final_total = models.DecimalField(max_digits=9, decimal_places=2, default=0.00)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return (self.order_id)

	def __str__(self):
		return (self.order_id)