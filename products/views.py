# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.db.models import Q

# Create your views here.

from django.views.generic import TemplateView,ListView,DetailView
from .models import Product
# from carts.models import Cart

class HomeView(TemplateView):
	template_name = 'index.html'
	# queryset = Product.objects.all()
	# model = Product

	def get_context_data(self, **kwargs):
	    context = super(HomeView, self).get_context_data(**kwargs)
	    # context['products'] = self.queryset
	    return context

class ProductListView(ListView):
	# template_name = 'products/product_list.html'
	template_name = 'catalog.html'
	model = Product

	def get_context_data(self, **kwargs):
	    context = super(ProductListView, self).get_context_data(**kwargs)
	    context['title'] = 'Catalog'
	    return context


class ProductDetailView(DetailView):
	template_name = 'detail.html'
	model = Product

	# print dir(object)
	# def get_slug_field(self, **kwargs):
	# 	slug = super(ProductDetailView, self).get_slug_field(**kwargs)
	# 	print dir(slug)
	# 	return slug


def search_view(request):
	# print request.GET.get('q')
	# search code
	search = request.GET.get("q")
	queryset_list = Product.objects.all()
	if search:
		queryset_list = queryset_list.filter(
							Q(name__icontains=search)|
							Q(description__icontains=search)
							).distinct()
	print queryset_list
	context = {
		'result' : queryset_list
	}
	return render(request,'search_list.html',context)