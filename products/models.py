# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.urlresolvers import reverse

from django.db import models

# Create your models here.
class Product(models.Model):
	name = models.CharField(max_length=50, unique=True)
	slug = models.SlugField(max_length=100, unique=True, help_text='Unique value for product page URL, created from name.')
	price = models.DecimalField(max_digits=9,decimal_places=2)
	sale_price = models.DecimalField(max_digits=9,decimal_places=2)
	is_active = models.BooleanField(default=True)
	is_new_arrival = models.BooleanField(default=False)
	is_best_seller = models.BooleanField(default=False)
	is_featured = models.BooleanField(default=False)
	description = models.TextField()
	# image = models.ImageField(upload_to='products/images',null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	# categories = models.ManyToManyField(Category)

	class Meta:
		db_table = 'products'
		ordering = ['-created_at']
		unique_together = ('name', 'slug')

	def __unicode__(self):
		return self.name

	def __str__(self):
		return self.name

	def get_absolute_url(self):
		return reverse("products:product_detail", kwargs={"slug": self.slug})

	def get_price(self):
		return self.price
		# if self.old_price > self.price:
		# 	return self.price
		# else:
		# 	return None

class ProductImage(models.Model):
	product = models.ForeignKey(Product)
	image = models.ImageField(upload_to='products/images',null=True)
	is_featured = models.BooleanField(default=False)
	is_featured = models.BooleanField(default=False)
	is_thumbnail = models.BooleanField(default=False)
	updated_at = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return self.product.name

	def __str__(self):
		return self.product.name

class VariationManager(models.Manager):
	def all(self):
		return super(VariationManager,self).filter(is_active=True)

	def sizes(self):
		return self.all().filter(category='size')

	def colors(self):
		return self.all().filter(category='color')

	def packages(self):
		return self.all().filter(category='package')

VAR_CATEGORIES = (
	('size','size'),
	('color','color'),
	('package','package')
	)
class Variation(models.Model):
	product = models.ForeignKey(Product)
	category = models.CharField(max_length=20, choices=VAR_CATEGORIES, default='size')
	title = models.CharField(max_length=100)
	image = models.ForeignKey(ProductImage, null=True, blank=True)
	price = models.DecimalField(max_digits=9,decimal_places=2,null=True,blank=True)
	is_active = models.BooleanField(default=True)
	updated_at = models.DateTimeField(auto_now=True)
	# sku (stock keeping unit)

	objects = VariationManager()

	def __unicode__(self):
		return self.title

	def __str__(self):
		return self.title