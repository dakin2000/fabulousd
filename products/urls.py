from django.conf.urls import url
from django.contrib import admin

from .views import HomeView,ProductListView,ProductDetailView,search_view

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^s/$', search_view, name='search'),
    url(r'^products/$', ProductListView.as_view(), name='product_list'),
    url(r'^products/(?P<slug>[\w-]*)/$', ProductDetailView.as_view(), name='product_detail'),
    # url(r'^products/$', CatalogDetailView.as_view(), name='detail'),
    # url(r'^catalog/$', CatalogView.as_view(), name='catalog'),
    # url(r'^catalog/(?P<slug>[w]+)$', DetailView.as_view(), name='detail')
]
