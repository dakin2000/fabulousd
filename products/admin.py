# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Product,ProductImage,Variation

class ProductAdmin(admin.ModelAdmin):
	list_display = ('name', 'price', 'is_active', 'created_at', 'updated_at',)
	list_display_links = ('name',)
	list_per_page = 50
	ordering = ['-created_at']
	search_fields = ['name', 'description']
	exclude = ('created_at', 'updated_at',)
	# sets up slug to be generated from product name
	prepopulated_fields = {'slug' : ('name',)}
	list_editable = ['price', 'is_active']
	list_filter = ['price', 'is_active']
	readonly_fields = ['created_at', 'updated_at']

	class Meta:
		model = Product


admin.site.register(Product,ProductAdmin)
admin.site.register(ProductImage)
admin.site.register(Variation)